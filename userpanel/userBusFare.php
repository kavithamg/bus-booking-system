<?php
  session_start();
  if (!isset($_SESSION["user"])) {
    header('Location: ../user.php');
    exit;
  }
  if($_SESSION["bus_id"] == "")
  {
    header("Location: userBus.php");
    exit();
  }
  $route = $_SESSION["route"];
  $busId =  $_SESSION["bus_id"];
  $seats = $_SESSION["seats"]; 
  $disabled = $err = $seatNum = "";
  $fare = 0;
  $count = 0;
  require_once('../mysqldb.php');
  if($_SERVER['REQUEST_METHOD'] != "POST")
  {
    $sql = "select fare_per_seat,tourdate, busName, dept_time, busType, arri_time from businfo where busID = '$busId';";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    $fare = $row["fare_per_seat"];
    $_SESSION["perfare"] = $fare;
    $_SESSION["jdate"] = $row["tourdate"];
    $_SESSION["jtime"] = $row["dept_time"]." - ".$row["arri_time"];
    $_SESSION["v_name"] = $v_name = $row["busName"];
    $_SESSION["category"] = $category = $row["busType"];
    $seatNum = "";
    $first = true;
    foreach($seats as $seat) 
    {
      if($first)
      {
        $seatNum = $seat;
        $first = false;
      }
      else
        $seatNum .= ", ".$seat;
      $count++;
      $_SESSION["seatNum"] = $seatNum;
    } $_SESSION["fare"] = $count*$_SESSION["perfare"];
  } 
 else{ 
    $first = true;
    foreach($seats as $seat)
    {
      if($first)
      {
        $seatNum = $seat;
        $first = false;
      }
      else
        $seatNum .= ", ".$seat;
      $_SESSION["seatNum"] = $seatNum;
      $count++;
    }
  }
  if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST['submit']))
  { 
    if($_POST["amount"] == $_SESSION["fare"])
    {
      foreach ($seats as $seat) {
      $sql = "insert into seat_info (seat_num, busID, status) values ('$seat', '$busId', 'checked')";
      if(!mysql_query($sql))
      {
        echo mysql_error($conn);
      }
    }
    $sql = "update businfo set avai_seat = avai_seat-$count where busID=$busId;";
     if(!mysql_query($sql))
        $err = mysql_error();
		//*********************//
     $sql5 = "SELECT * FROM user_info";
           $result5 = mysql_query($sql5);
            while($row = mysql_fetch_array($result5))
            {
              $u_id= $row['email'];
              if($_SESSION["user"] == $u_id)
              {
                $getUID = $row['user_id'];
                $getName = $row['name'];
                $getGen = $row['gender'];
                $getPhn = $row['phone'];
              }   
            }
     //********************//
     //*****************//
      $_SESSION["payment"] = $payment = $_REQUEST['payMethod'];
      $_SESSION["amount"] = $amount = $_REQUEST['amount'];
      $_SESSION["jdate"] = $jdate = $_SESSION["jdate"];
      $_SESSION["jtime"] = $jtime = $_SESSION["jtime"];
      $v_name = $_SESSION["v_name"];
      $category = $_SESSION["category"];
      $busId =  $_SESSION["bus_id"];
      $flightUserID =  $_SESSION["user_id"];
      $flightUserName =  $_SESSION["name"];
      $flightUserPhone =  $_SESSION["phone"];
      $sql1 = "insert into user_ticket_info (user_id,busID,vehicle_name, pay_method, amount, seats, busType, route, jdate, jtime) values ('$getUID','$busId','$v_name','$payment','$amount','$seatNum','$category','$route','$jdate','$jtime');";
      if(!mysql_query($sql1))
        echo mysql_error();
      else
        {
          $sql = "SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'reservation_system' AND TABLE_NAME = 'ticket_invoice';";
          $result = mysql_query($sql);
          $row = mysql_fetch_array($result);
          $_SESSION["ticket_id"] = $row[0]-1;        
        
          //****************//
         header('Location: userBusTicket.php');
         exit(); 
       }
    }
    else
    {
      $err = "Give Equal Amount of Money, Please!";
    }
    
  }
  
  /*if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["checkcoupon"]))
  {
    $coupon = $_POST["coupon"];
    $sql = "select * from coupon_info where coupon_num = '$coupon'";
    $result = mysql_query($sql);
    $isvalid = false;
    while($row = mysql_fetch_array($result)){
      $discount = $row["discount"];
      $f = $count * $_SESSION["perfare"];
      $_SESSION["fare"] = $f-($f * ($discount/100));    
      $disabled = "disabled";
      $isvalid = true;
      $err = "";
    }
    if(!$isvalid)
      $err = "Invalid coupon";
  }*/

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>TourBus | Online Bus reservation</title>
		<meta charset="utf-8">
		<meta name = "format-detection" content = "telephone=no" />
		<link rel="icon" href="../images/favicon.ico">
		<link rel="shortcut icon" href="../images/favicon.ico" />
		<link rel="stylesheet" href="../booking/css/booking.css">
		<link rel="stylesheet" href="../css/camera.css">
		<link rel="stylesheet" href="../css/owl.carousel.css">
		<link rel="stylesheet" href="../css/style.css">
		<script src="../js/jquery.js"></script>
		<script src="../js/jquery-migrate-1.2.1.js"></script>
		<script src="../js/script.js"></script>
		<script src="../js/superfish.js"></script>
		<script src="../js/jquery.ui.totop.js"></script>
		<script src="../js/jquery.equalheights.js"></script>
		<script src="../js/jquery.mobilemenu.js"></script>
		<script src="../js/jquery.easing.1.3.js"></script>
		<script src="../js/owl.carousel.js"></script>
		<script src="../js/camera.js"></script>
		<!--[if (gt IE 9)|!(IE)]><!-->
		<script src="../js/jquery.mobile.customized.min.js"></script>
		<!--<![endif]-->
		<script src="../booking/js/booking.js"></script>
		<style>
				.button {
						background-color: #fdc903;
						border: none;
						color: white;
						padding: 15px 32px;
						text-align: center;
						text-decoration: none;
						display: inline-block;
						font-size: 16px;
						margin: 4px 2px;
						cursor: pointer;
						-webkit-transition-duration: 0.4s; /* Safari */
						transition-duration: 0.4s;
				}

				.button2:hover {
						box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
						border-radius: 25px;
				}
				
				#bus input[type=text] {
						width: 100%;
						padding: 12px 20px;
						margin: 8px 0;
						box-sizing: border-box;
				}
		</style>
		<script>
			$(document).ready(function(){
				jQuery('#camera_wrap').camera({
					loader: false,
					pagination: false ,
					minHeight: '444',
					thumbnails: false,
					height: '28.28125%',
					caption: true,
					navigation: true,
					fx: 'mosaic'
				});
				$().UItoTop({ easingType: 'easeOutQuart' });
			});
		</script>
		<!--[if lt IE 8]>
			<div style=' clear: both; text-align:center; position: relative;'>
				<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
					<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
				</a>
			</div>
			<![endif]-->
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
	</head>
    <body class="page1" id="top">
		<div class="main">
<!--==============================header=================================-->
			<header>
				<div class="menu_block ">
					<div class="container_12">
						<div class="grid_12">
							<nav class="horizontal-nav full-width horizontalNav-notprocessed">
								<ul class="sf-menu">
									<li><a href="user.php">About</a></li>
									<li class="current"><a href="userBus.php">Bus</a></li>
									<li><a href="../logout.php">Logout</a></li>
								</ul>
							</nav>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</header>
			<div class="clear"></div>
			<br />
			<br />
			<br /> 
     <center>
		<div class="content"><div class="ic"></div>
				<div class="container_12">
				<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
     <table>
       <tr>
         <th style="font-weight:bold; font-size: 20px;">Your Seat No.: </th>
         <td style="font-weight:bold; font-size: 20px;"> &nbsp;<?php echo $seatNum; ?></td>
       </tr>
       <tr>
         <th style="font-weight:bold; font-size: 20px;">Total Fare: </th>
         <td style="font-weight:bold; font-size: 20px;"> INR <?php echo $_SESSION["fare"]; ?></td>
       </tr>
     </table>
     </form>
     <span style="color:red"> <?php echo $err; ?> </span>
<h3 style="color: coral;"><u>Please Provide Your Informations</u></h3>
       <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post" id="bus">
           <table>
              <tr>
                <th>Full Name: </th>
                <td><input type="text" id="fname" name="fname" pattern=".{3,15}" title="3-15 characters" size="35" placeholder="write full name, Maximum(15 Char)" required></td>
              </tr>
			 
              <tr>
                <th>Gender: </th>
                <td><input type="radio" id="gender" name="gender" value="Male" checked="checked"> Male<input type="radio" id="gender" name="gender" value="Female"> Female<input type="radio" id="gender" name="gender" value="Other"> Other
                </td>
              </tr>
              <tr>
               <th>Email: </th>
               <td><input type="text" name="email" id="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required placeholder="Enter a valid email" title="Input valid email,please!" size="35" /></td>
             </tr>
              <tr>
                <th>Phone: </th> 
                <td><input type="text" id="phone" name="phone" size="35" pattern=".{11,16}" title="valid phone number" placeholder="Enter your Contact No." required></td>
            </tr>
            <tr>
                <th>Payment By: </th> 
                <td>
                  <select name="payMethod" required>
                      <option value="" disabled="disabled" selected="selected">Select PaymentGetway</option>
                      <option value="Cash on Board">Cash on Board</option>
                      <option value="Net Banking">Net Banking</option>
                      <option value="Debit Card">Debit Card</option>
                  </select>
                </td>
            </tr>
            <tr>
                <th>Enter Amount: </th> 
                <td><input type="text" id="amount" name="amount" size="35" required></td>
            </tr>
            <tr>
              <th></th>
              <td><input type="submit" name="submit" value="Continue" class="button button2" /></td>
              </tr>
        </table>
        </form>
				    
					<div class="clear"></div>
				</div>
        </div>
  <br/> <br/>
     </center>
    
<?php
   include('../footer.php');
?>