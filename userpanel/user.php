<?php
session_start();
  if (!isset($_SESSION["user"])) {
    header('Location: ../home.php');
    exit;
  }
 
 require_once('../mysqldb.php');
  $sql1 = "SELECT * FROM user_info";
  $result1 = mysql_query($sql1);
  while($row = mysql_fetch_array($result1))
  {
    $u_id = $row['email'];
    if($_SESSION["user"] == $u_id)
    {
      $getUID = $row['user_id'];
      $getName = $row['name'];
      $getEmail = $row['email'];
      $getGen = $row['gender'];
      $getPhn = $row['phone'];
	  $getPass = $row['password'];
    }   
  }
  $_SESSION["UserID"] =  $getUID;
  $sql = "SELECT * FROM login where acctype = 'User'";
  $result = mysql_query($sql);
  $nn = mysql_num_rows($result);
     if($nn != 0)
       {
         while($row1 = mysql_fetch_array($result))
          {
           $up_id= $row1['email'];
     if($_SESSION["user"] == $up_id)
       {
          $getPass= $row1['pass'];
        }
    }
 }
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>TourBus | Online Bus reservation</title>
		<meta charset="utf-8">
		<meta name = "format-detection" content = "telephone=no" />
		<link rel="icon" href="../images/favicon.ico">
		<link rel="shortcut icon" href="../images/favicon.ico" />
		<link rel="stylesheet" href="../booking/css/booking.css">
		<link rel="stylesheet" href="../css/camera.css">
		<link rel="stylesheet" href="../css/owl.carousel.css">
		<link rel="stylesheet" href="../css/style.css">
		<script src="../js/jquery.js"></script>
		<script src="../js/jquery-migrate-1.2.1.js"></script>
		<script src="../js/script.js"></script>
		<script src="../js/superfish.js"></script>
		<script src="../js/jquery.ui.totop.js"></script>
		<script src="../js/jquery.equalheights.js"></script>
		<script src="../js/jquery.mobilemenu.js"></script>
		<script src="../js/jquery.easing.1.3.js"></script>
		<script src="../js/owl.carousel.js"></script>
		<script src="../js/camera.js"></script>
		<!--[if (gt IE 9)|!(IE)]><!-->
		<script src="../js/jquery.mobile.customized.min.js"></script>
		<!--<![endif]-->
		<script src="../booking/js/booking.js"></script>
		<style>
			#bus {
					font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
					border-collapse: collapse;
					width: 100%;
			}

		#bus td, #bus th {
					border: 1px solid #ddd;
					padding: 8px;
					text-align: center;
					font-size: 20px;
		}

		#bus tr:nth-child(even){background-color: #f2f2f2;}

		#bus tr:hover {background-color: #ddd;}

		#bus th {
				width: 25%;
				padding-top: 12px;
				padding-bottom: 12px;
				text-align: left;
				background-color: #fdc903;
				color: white;
				font-size: 20px;
				font-weight: bold;
				text-align: center;
		}
		
		#detail input[type=text] {
					width: 100%;
					padding: 12px 20px;
					margin: 8px 0;
					box-sizing: border-box;
					border: none;
					font-size: 20px;
		}
		
		.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
	}
	
	.button4 {
    background-color: white;
    color: black;
    border: 2px solid #e7e7e7;
}

.button4:hover {background-color: #e7e7e7;}
		</style>
		<script>
			$(document).ready(function(){
				jQuery('#camera_wrap').camera({
					loader: false,
					pagination: false ,
					minHeight: '444',
					thumbnails: false,
					height: '28.28125%',
					caption: true,
					navigation: true,
					fx: 'mosaic'
				});
				$().UItoTop({ easingType: 'easeOutQuart' });
			});
		</script>
		<!--[if lt IE 8]>
			<div style=' clear: both; text-align:center; position: relative;'>
				<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
					<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
				</a>
			</div>
			<![endif]-->
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
	</head>
  <body class="page1" id="top">
		<div class="main">
<!--==============================header=================================-->
			<header>
				<div class="menu_block ">
					<div class="container_12">
						<div class="grid_12">
							<nav class="horizontal-nav full-width horizontalNav-notprocessed">
								<ul class="sf-menu">
									<li class="current"><a href="user.php">About</a></li>
									<li><a href="userBus.php">Bus</a></li>
									<li><a href="../logout.php">Logout</a></li>
								</ul>
							</nav>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</header>
			<div class="clear"></div>
			<br />
			<br />
			<br /> 
			<center>
			<u><h3 style="color: coral;">Here,You Can Update Your Information!</h3></u>
			<div class="content"><div class="ic"></div>
				<div class="container_12">
						<div>
							<form action="uinfo_update.php" method="POST" id="detail">
								<table id="bus">
										<input type="hidden" name="uid" readonly value= "<?php echo $getUID; ?>"/>
										<tr>
											<th>Name</th>
											<td><input type="text" name="uname" value="<?php echo $getName; ?>" required /></td>
										</tr>
										<tr>
											<th>Gender</th>
											<td><input type="text" name="gender" disabled value= "<?php echo $getGen; ?>" required/></td>
										</tr>
										<tr>
											<th>Email</th>
											<td><input type="text" name="email" readonly value= "<?php echo $getEmail; ?>" required /></td>
										</tr>
										<tr>
											<th>Phone</th>
											<td><input type="text" name="phone" value= "<?php echo $getPhn; ?>" required /></td>
										</tr>
										<tr>
											<th>Password</th>
											<td><input type="text" name="password" value= "<?php echo $getPass; ?>" required /></td>
										</tr>
								</table>
								<br />
								<input type="submit" name="submit" value="Update" class="button button4"/>
							</form>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="content"><div class="ic"></div>
					<div class="container_12">
				     <?php
 $sql = "SELECT * FROM user_info, user_ticket_info where user_info.user_id = '$getUID' and user_ticket_info.user_id = '$getUID'";
              $result = mysql_query($sql);
              $i = 0;        
?>
 <form method="POST">
   <u><h3 style="color: coral; text-align: left;">Ticket (Bus)</h3></u>
      <table id="bus">
                 <tr>
                    <th>Date</th>
                    <th>Bus Name</th>
                    <th>Routes</th> 
                    <th>Category</th>
                    <th>Time</th>
                    <th>Seat Number</th>
                    <th>Fare</th>
                    <th>PaymentBy</th>
                    <th>Ticket No.</th>
                    <th>Bus No.</th>
                    <th>Print</th>
                 </tr> 
            <?php
              while($row = mysql_fetch_array($result)) {
               $ticketid = $row['ticket_id'];
               ?>             
               
                 <tr>
                    <td>
                    <?php echo $row['jdate']?>
                   </td>
                   <td>
                    <?php echo $row['vehicle_name']?>
                   </td>
                   <td>
                    <?php echo $row['route'] ?>
                   </td>
                   <td>
                     <?php echo $row['busType']?>
                   </td>
                   <td>
                     <?php echo $row['jtime']?>
                   </td>
                   <td>
                     <?php echo $row['seats']?>
                   </td>
                   <td>
                     <?php echo $row['amount']?>
                   </td>
                   <td>
                     <?php echo $row['pay_method']?>
                   </td>
                   <td>
                     <?php echo $row['ticket_id']?>
                   </td>
                   <td>
                     <?php echo $row['busID']?>
                   </td>
                   <td onclick="document.location = 'userBusPersonalTicket.php?id=<?php echo urlencode($ticketid);?>'" style="background-color: coral; color: white; font-size: 20px; cursor: pointer;">Print</td>
                </tr>                
          <?php
          }
        ?>
        </table>
   </form>
					<div class="clear"></div>
					</div>
				</div>
	</center>
  <br/> <br/>
<?php
    include('../footer.php');
?>