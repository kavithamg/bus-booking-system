<?php
  session_start();
  if (!isset($_SESSION["user"])) {
      header('Location: ../user.php');
      exit;
    }
  if($_SESSION["bus_id"] == "")
  {
    header("Location: userBus.php");
    exit();
  }
  require_once('../mysqldb.php');
  $busId =  $_SESSION["bus_id"];
  $sql5 = "SELECT * FROM user_info";
  $result5 = mysql_query($sql5);
  while($row = mysql_fetch_array($result5))
    {
      $u_id= $row['email'];
      if($_SESSION["user"] == $u_id)
      {
        $getName = $row['name'];
        $getGen = $row['gender'];
        $getEmail = $row['email'];
        $getPhn = $row['phone'];
      }   
    }
  $sql6 = "select * from user_ticket_info where busID = '$busId'";
  $result6 = mysql_query($sql6);
    while($row = mysql_fetch_array($result6))
    {
      $u_id= $row['user_id'];
      $u_seat = $row['seats'];
      if($_SESSION["user"] == $u_id && $_SESSION["seatNum"] == $u_seat)
      {
        $ticketID = $row['ticket_id'];
      }   
    }
?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<title>TourBus | Online Bus reservation</title>
		<meta charset="utf-8">
		<meta name = "format-detection" content = "telephone=no" />
		<link rel="icon" href="../images/favicon.ico">
		<link rel="shortcut icon" href="../images/favicon.ico" />
		<link rel="stylesheet" href="../booking/css/booking.css">
		<link rel="stylesheet" href="../css/camera.css">
		<link rel="stylesheet" href="../css/owl.carousel.css">
		<link rel="stylesheet" href="../css/style.css">
		<script src="../js/jquery.js"></script>
		<script src="../js/jquery-migrate-1.2.1.js"></script>
		<script src="../js/script.js"></script>
		<script src="../js/superfish.js"></script>
		<script src="../js/jquery.ui.totop.js"></script>
		<script src="../js/jquery.equalheights.js"></script>
		<script src="../js/jquery.mobilemenu.js"></script>
		<script src="../js/jquery.easing.1.3.js"></script>
		<script src="../js/owl.carousel.js"></script>
		<script src="../js/camera.js"></script>
		<!--[if (gt IE 9)|!(IE)]><!-->
		<script src="../js/jquery.mobile.customized.min.js"></script>
		<!--<![endif]-->
		<script src="../booking/js/booking.js"></script>
		<style>
			#bus {
					font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
					border-collapse: collapse;
					width: 100%;
			}

		#bus td, #bus th {
					border: 1px solid #ddd;
					padding: 8px;
					text-align: center;
					font-size: 20px;
		}

		#bus tr:nth-child(even){background-color: #f2f2f2;}

		#bus tr:hover {background-color: #ddd;}

		#bus th {
				width: 25%;
				padding-top: 12px;
				padding-bottom: 12px;
				text-align: left;
				background-color: #fdc903;
				color: white;
				font-size: 20px;
				font-weight: bold;
				text-align: center;
		}
		
		#print:link, #print:visited {
    background-color: white;
    color: black;
    border: 2px solid green;
    padding: 10px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
}

#print:hover, #print:active {
    background-color: green;
    color: white;
}	
		</style>
		<script>
			$(document).ready(function(){
				jQuery('#camera_wrap').camera({
					loader: false,
					pagination: false ,
					minHeight: '444',
					thumbnails: false,
					height: '28.28125%',
					caption: true,
					navigation: true,
					fx: 'mosaic'
				});
				$().UItoTop({ easingType: 'easeOutQuart' });
			});
		</script>
		<!--[if lt IE 8]>
			<div style=' clear: both; text-align:center; position: relative;'>
				<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
					<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
				</a>
			</div>
			<![endif]-->
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
	</head>
<body class="page1" id="top">
		<div class="main">
<!--==============================header=================================-->
			<header>
				<div class="menu_block ">
					<div class="container_12">
						<div class="grid_12">
							<nav class="horizontal-nav full-width horizontalNav-notprocessed">
								<ul class="sf-menu">
									<li><a href="user.php">About</a></li>
									<li class="current"><a href="userBus.php">Bus</a></li>
									<li><a href="../logout.php">Logout</a></li>
								</ul>
							</nav>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</header>
			<div class="clear"></div>
			<br />
			<br />
			<br /> 
    <center> 	  
	  <div class="content"><div class="ic"></div>
				<div class="container_12">
				<h3 style="color: coral"><u>Your Ticket Informations</u></h3>
    
      <table id="bus">
        <tr>
          <th>Ticket No.: </th>
          <td><?php echo $ticketID ?></td>
        </tr>
        <tr>
          <th>Bus No.: </th>
          <td><?php echo $_SESSION["bus_id"] ?></td>
        </tr>
        <tr>
          <th>Your Name: </th>
          <td><?php echo $_SESSION["fname"] ?></td>
        </tr>
        <tr>
          <th>Your Gender: </th>
          <td><?php echo $_SESSION["gender"] ?></td>
        </tr>
        <tr>
          <th>Your Email: </th>
          <td><?php echo $_SESSION["email"] ?></td>
        </tr>
        <tr>
          <th>Your Phone: </th>
          <td><?php echo $_SESSION["phone"] ?></td>
        </tr>
         <tr>
          <th>Vehicle Name: </th>
          <td><?php echo $_SESSION["v_name"] ?></td>
        </tr>
        <tr>
          <th>Journey Date: </th>
          <td><?php echo $_SESSION["jdate"] ?></td>
        </tr>
        <tr>
          <th>Time: </th>
          <td><?php echo $_SESSION["jtime"] ?></td>
        </tr>
        <tr>
          <th>Route: </th>
          <td><?php echo $_SESSION["route"] ?></td>
        </tr>
        <tr>
          <th>Your Seat: </th>
          <td><?php echo $_SESSION["seatNum"] ?></td>
        </tr>
        <tr>
          <th>Seat Category: </th>
          <td><?php echo $_SESSION["category"] ?></td>
        </tr>
        <tr>
          <th>Fare: </th>
          <td><?php echo $_SESSION["amount"] ?></td>
        </tr>
        <tr>
          <th>Payment By: </th>
          <td><?php echo $_SESSION["payment"] ?></td>
        </tr>  
      </table>
	  <br />
      <a href="userBus_t_print.php"  id="print">Print Your Ticket</a>
				    
					<div class="clear"></div>
				</div>
        </div>

    </center>
    <br/> <br/>
<?php
    include('../footer.php');
?>