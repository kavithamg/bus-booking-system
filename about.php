 </body>
</html>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>TourBus | Online Bus reservation</title>
		<meta charset="utf-8">
		<meta name = "format-detection" content = "telephone=no" />
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico" />
		<link rel="stylesheet" href="booking/css/booking.css">
		<link rel="stylesheet" href="css/camera.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<link rel="stylesheet" href="css/style.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.2.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/owl.carousel.js"></script>
		<script src="js/camera.js"></script>
		<!--[if (gt IE 9)|!(IE)]><!-->
		<script src="js/jquery.mobile.customized.min.js"></script>
		<!--<![endif]-->
		<script src="booking/js/booking.js"></script>
		<script>
			$(document).ready(function(){
				jQuery('#camera_wrap').camera({
					loader: false,
					pagination: false ,
					minHeight: '444',
					thumbnails: false,
					height: '28.28125%',
					caption: true,
					navigation: true,
					fx: 'mosaic'
				});
				$().UItoTop({ easingType: 'easeOutQuart' });
			});
		</script>
		<!--[if lt IE 8]>
			<div style=' clear: both; text-align:center; position: relative;'>
				<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
					<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
				</a>
			</div>
			<![endif]-->
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
	</head>
<body class="page1" id="top">
		<div class="main">
<!--==============================header=================================-->
			<header>
				<div class="menu_block ">
					<div class="container_12">
						<div class="grid_12">
							<nav class="horizontal-nav full-width horizontalNav-notprocessed">
								<ul class="sf-menu">
									<li><a href="bus.php">Home</a></li>
									<li class="current"><a href="about.php">About</a></li>
									<li><a href="login.php">Login/Signup</a></li>
								</ul>
							</nav>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</header>
			<div class="clear"></div>
			<div class="content"><div class="ic"></div>
				<div class="container_12">
				    <center>
							<h3>Details About My Project & About Me</h3>
							<div class="text1 color2">
								Online Bus Reservation System
							</div>
							<p>Online Bus Reservation System is a specialized business function that balances people need with corporate goals, financial and otherwise. 
							Online Bus Reservation System ensures cost tracking and control, facilitates adherence to corporate travel policies, realizes savings through negotiated discounts, and serves as a valuable information center for employees and managers in times when travel is not as smooth and carefree as it used to be.
							<p>But Online Bus Reservation System is still misunderstood and undervalued by some corporations today. Therefore, the role of business travel within the corporate structure must be placed in proper context so that its value can be measured and appreciated.</p></p>
							
							<br />
							<br />
							<div class="text1 color2">
								Requirement Specification
							</div>
							<h4>Purpose:</h4>
							<p>Transportation is one of the main key element of basic needs and development for a country. Developed Countries are more advanced in their transportation Management system. As our country India is a developing country it has become more important to improve this system and how we think of traveling here. For going to buy tickets from any ticket counter is not always pleasant and convenient for all people moreover its time consuming too. Online Ticket Booking system is the Beginning of new era which going to open the door with enormous possibilities.
								This system is a browser based system that is designed to store, process, retrieve and analyze information concerned with the administrative and inventory management within a Ticket Management System. This project aims at maintaining all the information pertaining to Traveler, different types of vehicle available in travel management system and helps them manage in a better way. The entire project will be developed keeping in view of the distributed client server computing technology. This reduces the paper work and leads to a paperless office.
								Through this website any traveler or passenger who is interested in traveling can register himself. Moreover, if any general traveler wants to make request ticket online he can also take the help of this site. Admin is the main authority who can do addition, deletion and modification if required.
								This system is futuristic, scalable and customizable, easy to use and user friendly, organized, simple. This system can maintain unlimited number of traveler information and the complete history will be available.
							</p>
							<br />
							</br />
							<h4>Project Scope: </h4>
							<p>
								The project’s focus is the development of Travel management system. The main deliverable of the project is the following:<br />
								- A detailed requirements analysis.<br />
								- Detailed data and process models of the system.<br />
								- The system itself.<br />
								- User manual.<br />
							</p>
							<br />
							<br />
							<div class="text1 color2">
								About Me
							</div>
							<h4>Name: <span style="font-weight: bold; font-size:15px;">Kavitha Megalingam</span> </h4>
							<h4>Roll No.: 17</h4>
							<h4 style="font-weight: bold; font-size:15px;">FYMCA </h4>
							<h4>Project Name: <span style="font-weight: bold; font-size:15px;">Online Bus Reservation System</span> </h4>
							
</center>
					<div class="clear"></div>
				</div>
        </div>
  <br/> <br/>
<?php
    include('footer.php');
?>