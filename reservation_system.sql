-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2016 at 06:14 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reservation_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adminId` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `contact_no` varchar(28) NOT NULL,
  `reg_date` date NOT NULL,
  `admin_type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `businfo`
--

CREATE TABLE `businfo` (
  `busID` int(11) UNSIGNED NOT NULL,
  `busName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `busType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tourdate` varchar(255) NOT NULL,
  `fare_per_seat` bigint(100) NOT NULL,
  `dept_time` varchar(255) NOT NULL,
  `arri_time` varchar(255) NOT NULL,
  `destination` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `avai_seat` bigint(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `businfo`
--

INSERT INTO `businfo` (`busID`, `busName`, `busType`, `source`, `tourdate`, `fare_per_seat`, `dept_time`, `arri_time`, `destination`, `avai_seat`) VALUES
(1, 'Ram Travels', 'AC Sleeper', 'Mumbai', '07/12/2016', 1000, '02:00:00', '05:00:00', 'Nashik', 29),
(2, 'ABC Travels', 'AC', 'Mumbai', '07/12/2016', 1000, '2:00 PM', '3:00 AM', 'Chennai', 32),
(4, 'Happy Travels', 'AC Sleeper', 'Mumbai', '08/12/2016', 800, '2:00 PM', '5:00 AM', 'Jaipur', 38),
(6, 'Laksh Travels', 'AC Sleeper', 'chennai', '07/12/2016', 2500, '2:00 PM', '10:00 AM', 'Mumbai', 35),
(7, 'Govind travels', 'AC Semi-Sleeper', 'Nashik', '09/12/2016', 1000, '6:00 PM', '10:00 PM', 'Mumbai', 39),
(10, 'lakshmi travels', 'NonAC Sleeper', 'Jaipur', '10/12/2016', 1500, '5:20 PM', '7:50 AM', 'Mumbai', 41),
(11, 'Guru Travels', 'AC Sleeper', 'Mumbai', '10/12/2016', 2500, '7:00 AM', '2:00 PM', 'Surat', 41);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(255) NOT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `acctype` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seat_info`
--

CREATE TABLE `seat_info` (
  `seat_num` varchar(100) NOT NULL,
  `busID` int(10) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seat_info`
--

INSERT INTO `seat_info` (`seat_num`, `busID`, `status`) VALUES
('A1', 2, 'checked'),
('A3', 1, 'checked'),
('A4', 1, 'checked'),
('B2', 1, 'checked'),
('B3', 1, 'checked'),
('B4', 2, 'checked'),
('D1', 6, 'checked'),
('D2', 2, 'checked'),
('D3', 1, 'checked'),
('D4', 2, 'checked'),
('E1', 1, 'checked'),
('E3', 6, 'checked'),
('F3', 1, 'checked'),
('G3', 6, 'checked');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_invoice`
--

CREATE TABLE `ticket_invoice` (
  `ticket_id` int(11) NOT NULL,
  `UserName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `busID` int(11) NOT NULL,
  `pay_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) NOT NULL,
  `seats` varchar(100) NOT NULL,
  `busType` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `jdate` varchar(255) NOT NULL,
  `jtime` varchar(255) NOT NULL,
  `busName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_invoice`
--

INSERT INTO `ticket_invoice` (`ticket_id`, `UserName`, `email`, `phone`, `gender`, `busID`, `pay_method`, `amount`, `seats`, `busType`, `route`, `jdate`, `jtime`, `busName`) VALUES
(1, 'Venkatesh', 'venkatesh.ksnadar@gmail.com', '12234567525', 'Male', 1, 'Bkash', '2000', 'A3, A4', 'AC Sleeper', 'Mumbai - Nashik', '07/12/2016', '02:00:00 - 05:00:00', 'Ram Travels'),
(2, 'venkatesh', 'venkatesh@gmail.com', '12345678910', 'Male', 1, 'DBBL', '2000', 'B2, B3', 'AC Sleeper', 'Mumbai - Nashik', '07/12/2016', '02:00:00 - 05:00:00', 'Ram Travels'),
(3, 'venkatesh', 'venkatesh@gmail.com', '12345678910', 'Male', 1, 'DBBL', '1000', 'D3', 'AC Sleeper', 'Mumbai - Nashik', '07/12/2016', '02:00:00 - 05:00:00', 'Ram Travels'),
(4, 'venkatesh', 'venkatesh@gmail.com', '12345678910', 'Male', 2, 'DBBL', '1000', 'A1', 'AC', 'Mumbai - Chennai', '07/12/2016', '2:00 PM - 3:00 AM', 'ABC Travels'),
(5, 'venkatesh', 'venkatesh@gmail.com', '21345678945', 'Male', 4, 'M-Cash', '800', 'B2', 'AC Sleeper', 'Mumbai - Jaipur', '08/12/2016', '2:00 PM - 5:00 AM', 'Happy Travels'),
(6, 'venkatesh', 'venkatesh@gmail.com', '21345678945', 'Male', 4, 'DBBL', '800', 'B2', 'AC Sleeper', 'Mumbai - Jaipur', '08/12/2016', '2:00 PM - 5:00 AM', 'Happy Travels'),
(7, 'venkatesh', 'venkatesh@gmail.com', '982394572210', 'Male', 1, 'DBBL', '1000', 'E1', 'AC Sleeper', 'Mumbai - Nashik', '07/12/2016', '02:00:00 - 05:00:00', 'Ram Travels'),
(8, 'venkatesh', 'venkatesh@gmail.com', '21345678945', 'Male', 2, 'Bkash', '2000', 'B3, B4', 'AC', 'Mumbai - Chennai', '07/12/2016', '2:00 PM - 3:00 AM', 'ABC Travels'),
(9, 'kavi', 'kavitha@gmail.com', '21345678945', 'Female', 6, 'Bkash', '2500', 'A3', 'AC Sleeper', 'chennai - Mumbai', '07/12/2016', '2:00 PM - 10:00 AM', 'Laksh Travels'),
(10, 'kavi', 'kavitha@gmail.com', '21345678945', 'Female', 6, 'Bkash', '2500', 'A3', 'AC Sleeper', 'chennai - Mumbai', '07/12/2016', '2:00 PM - 10:00 AM', 'Laksh Travels'),
(11, 'kavi', 'kavitha@gmail.com', '21345678945', 'Female', 2, 'Cash on Board', '1000', 'D2', 'AC', 'Mumbai - Chennai', '07/12/2016', '2:00 PM - 3:00 AM', 'ABC Travels'),
(12, 'venkatesh', 'venkatesh@gmail.com', '21345678945', 'Male', 7, 'Cash on Board', '2000', 'D1, D2', 'AC Semi-Sleeper', 'Nashik - Mumbai', '09/12/2016', '6:00 PM - 10:00 PM', 'Govind travels'),
(13, 'kavitha', 'kavitha@gmail.com', '21345678945', 'Female', 6, 'Cash on Board', '2500', 'G3', 'AC Sleeper', 'chennai - Mumbai', '07/12/2016', '2:00 PM - 10:00 AM', 'Laksh Travels');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `UserID` int(11) NOT NULL,
  `UserName` varchar(255) CHARACTER SET utf16 COLLATE utf16_unicode_ci NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `contact_no` varchar(28) NOT NULL,
  `acctype` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserID`, `UserName`, `email`, `password`, `contact_no`, `acctype`) VALUES
(1, 'admin', 'admin@example.com', 'admin', '956875642', 'Admin'),
(2, 'user', 'user@example.com', 'user123', '', 'User'),
(3, 'kavitha', 'kavitha@gmail.com', 'kavi123', '', 'User'),
(4, 'madhavi', 'madhavi@gmail.com', 'madhavi123', '21345678945', 'User'),
(5, 'kavi', 'kavi@gmail.com', 'kavi123', '21354555486', 'User'),
(6, 'vasantha', 'vasantha@gmail.com', 'vasantha123', '21354555486', 'User'),
(7, 'shanthi', 'shanthi@yahoo.com', 'shanthi123', '21354555486', 'User'),
(8, 'hari', 'hari@gmail.com', 'hari123', '21345678945', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `user_id`, `name`, `email`, `gender`, `phone`, `password`) VALUES
(4, 'user@example.com', 'user', 'user@example.com', 'Male', '21354555486', 'user123'),
(5, 'kavitha@gmail.com', 'kavitha', 'kavitha@gmail.com', 'Female', '21354555486', 'kavi123'),
(6, 'madhavi@gmail.com', 'madhavi', 'madhavi@gmail.com', 'Female', '21345678945', 'madhavi123'),
(7, 'kavi@gmail.com', 'kavi', 'kavi@gmail.com', 'Female', '21354555486', 'kavi123'),
(8, 'vasantha@gmail.com', 'vasantha', 'vasantha@gmail.com', 'Female', '21354555486', 'vasantha123'),
(9, 'shanthi@yahoo.com', 'shanthi', 'shanthi@yahoo.com', 'Female', '21354555486', 'shanthi123'),
(10, 'hari@gmail.com', 'hari', 'hari@gmail.com', 'Male', '21345678945', 'hari123');

-- --------------------------------------------------------

--
-- Table structure for table `user_ticket_info`
--

CREATE TABLE `user_ticket_info` (
  `id` int(11) NOT NULL,
  `busID` int(10) NOT NULL,
  `ticket_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) NOT NULL,
  `vehicle_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `jdate` varchar(100) NOT NULL,
  `jtime` varchar(100) NOT NULL,
  `route` varchar(255) NOT NULL,
  `seats` varchar(255) NOT NULL,
  `busType` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `pay_method` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_ticket_info`
--

INSERT INTO `user_ticket_info` (`id`, `busID`, `ticket_id`, `name`, `gender`, `email`, `phone`, `vehicle_name`, `jdate`, `jtime`, `route`, `seats`, `busType`, `amount`, `pay_method`, `user_id`) VALUES
(1, 4, '', '', '', '', '', 'Happy Travels', '08/12/2016', '2:00 PM - 5:00 AM', 'Mumbai - Jaipur', 'B2', 'AC Sleeper', '800', 'DBBL', 'user@example.com'),
(2, 2, '', '', '', '', '', 'ABC Travels', '07/12/2016', '2:00 PM - 3:00 AM', 'Mumbai - Chennai', 'D4', 'AC', '1000', 'M-Cash', 'user@example.com'),
(3, 2, '', '', '', '', '', 'ABC Travels', '07/12/2016', '2:00 PM - 3:00 AM', 'Mumbai - Chennai', 'D4', 'AC', '1000', 'M-Cash', 'user@example.com'),
(4, 2, '', '', '', '', '', 'ABC Travels', '07/12/2016', '2:00 PM - 3:00 AM', 'Mumbai - Chennai', 'D4', 'AC', '1000', 'M-Cash', 'user@example.com'),
(5, 6, '', '', '', '', '', 'Laksh Travels', '07/12/2016', '2:00 PM - 10:00 AM', 'chennai - Mumbai', 'D1, D4', 'AC Sleeper', '5000', 'Bkash', 'user@example.com'),
(6, 6, '', '', '', '', '', 'Laksh Travels', '07/12/2016', '2:00 PM - 10:00 AM', 'chennai - Mumbai', 'E3', 'AC Sleeper', '2500', 'Cash on Board', 'user@example.com'),
(7, 2, '', '', '', '', '', 'ABC Travels', '07/12/2016', '2:00 PM - 3:00 AM', 'Mumbai - Chennai', 'D3', 'AC', '1000', 'Cash on Board', 'user@example.com'),
(8, 1, '', '', '', '', '', 'Ram Travels', '07/12/2016', '02:00:00 - 05:00:00', 'Mumbai - Nashik', 'D2', 'AC Sleeper', '1000', 'Cash on Board', 'kavi@gmail.com'),
(9, 1, '', '', '', '', '', 'Ram Travels', '07/12/2016', '02:00:00 - 05:00:00', 'Mumbai - Nashik', 'F3', 'AC Sleeper', '1000', 'Cash on Board', 'kavi@gmail.com'),
(10, 2, '', '', '', '', '', 'ABC Travels', '07/12/2016', '2:00 PM - 3:00 AM', 'Mumbai - Chennai', 'E1', 'AC', '1000', 'Cash on Board', 'kavitha@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminId`),
  ADD UNIQUE KEY `adminId` (`adminId`);

--
-- Indexes for table `businfo`
--
ALTER TABLE `businfo`
  ADD PRIMARY KEY (`busID`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seat_info`
--
ALTER TABLE `seat_info`
  ADD PRIMARY KEY (`seat_num`);

--
-- Indexes for table `ticket_invoice`
--
ALTER TABLE `ticket_invoice`
  ADD PRIMARY KEY (`ticket_id`),
  ADD KEY `ticket_id` (`ticket_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`UserID`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `acctype` (`acctype`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_ticket_info`
--
ALTER TABLE `user_ticket_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `businfo`
--
ALTER TABLE `businfo`
  MODIFY `busID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ticket_invoice`
--
ALTER TABLE `ticket_invoice`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_ticket_info`
--
ALTER TABLE `user_ticket_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
