<?php
session_start();
  
require_once('mysqldb.php');
$err = "";
  if($_SERVER["REQUEST_METHOD"] == "POST")
    {
   $email = $_POST['email'];
   $password = $_POST['pass'];

  $sql = "SELECT * FROM user";
  $result = mysql_query($sql);
     
    while($row = mysql_fetch_array($result))
    {
       $un= $row['email'];
       $up= $row['password'];
       $acctype = $row['acctype'];
       
       if($un == $email && $up == $password && $acctype=="Admin")
      {
        $_SESSION["admin"] = $email;
        header('Location: adminpanel/index.php');
        exit;
      }
      else if($un == $email && $up == $password && $acctype=="User")
      {
        $_SESSION["user"] = $email;
        header('Location: userpanel/user.php');
        exit;      
      }
       else
      {
        $err = 'Invalid Email or Pass';
      }
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>TourBus | Online Bus reservation</title>
		<meta charset="utf-8">
		<meta name = "format-detection" content = "telephone=no" />
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico" />
		<link rel="stylesheet" href="booking/css/booking.css">
		<link rel="stylesheet" href="css/camera.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<link rel="stylesheet" href="css/style.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.2.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/owl.carousel.js"></script>
		<script src="js/camera.js"></script>
		<!--[if (gt IE 9)|!(IE)]><!-->
		<script src="js/jquery.mobile.customized.min.js"></script>
		<!--<![endif]-->
		<script src="booking/js/booking.js"></script>
		<style>
				.button {
						background-color: #4CAF50; /* Green */
						border: none;
						color: white;
						padding: 15px 32px;
						text-align: center;
						text-decoration: none;
						display: inline-block;
						font-size: 16px;
						margin: 4px 2px;
						cursor: pointer;
						-webkit-transition-duration: 0.4s; /* Safari */
						transition-duration: 0.4s;
				}

				.button2:hover {
						box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
				}
				
				#print:link, #print:visited {
    background-color: white;
    color: black;
    border: 2px solid #fdc903;
    padding: 10px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
}

#print:hover, #print:active {
    background-color: #fdc903;
    color: white;
}	
		
		</style>
		<script>
			$(document).ready(function(){
				jQuery('#camera_wrap').camera({
					loader: false,
					pagination: false ,
					minHeight: '444',
					thumbnails: false,
					height: '28.28125%',
					caption: true,
					navigation: true,
					fx: 'mosaic'
				});
				$().UItoTop({ easingType: 'easeOutQuart' });
			});
		</script>
		<!--[if lt IE 8]>
			<div style=' clear: both; text-align:center; position: relative;'>
				<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
					<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
				</a>
			</div>
			<![endif]-->
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
	</head>
<body class="page1" id="top">
		<div class="main">
<!--==============================header=================================-->
			<header>
				<div class="menu_block ">
					<div class="container_12">
						<div class="grid_12">
							<nav class="horizontal-nav full-width horizontalNav-notprocessed">
								<ul class="sf-menu">
									<li><a href="home.php">Home</a></li>
									<li><a href="about.php">About</a></li>
									<li class="current"><a href="login.php">Login/Signup</a></li>
								</ul>
							</nav>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</header>
			<div class="clear"></div>
			<br />
			<br />
			<br /> 
<center>
	  <div class="content"><div class="ic"></div>
				<div class="container_12">
				    <form id="bookingForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
					<div class="grid_4 prefix_4">
								<div class="tmInput">
									 <input type="text" placeHolder='Enter your email-id' name="email" id="email" required />
								</div>
								<br />
								<div class="tmInput">
									<input type="password" placeHolder='Enter your password' name="pass" id="pass" required />
								</div>
								<br />
								<div class="tmInput">
									<input type="submit" name="submit" value="Login" class="button button2"  style="background-color: #fdc903;"/>
				                </div>
								<br />
								<p style="color:red"><?php echo $err; ?></p>
				                <div class="tmInput ">
				                    <h2 style="color: coral;"><u>Not Registered Yet?</u></h2> &nbsp <br /> 
									<a href="signup.php" id="print">Sign Up Here.</a>
				                </div>
							</div>
                        </form>
					<div class="clear"></div>
				</div>
        </div>
</center>
<br/><br/><br/><br/>

<?php
    include('footer.php');
?>