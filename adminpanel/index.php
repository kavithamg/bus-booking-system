<?php
  session_start();
  if (!isset($_SESSION["admin"])) {
    header('Location: ../index.php');
    exit;
  }
  require_once('../mysqldb.php');
  $err = "";
   if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST['submit']))
      {    
        $date = $_REQUEST['datepicker'];
        $b_name = $_REQUEST['busName'];
        $dept_from = $_REQUEST['deptfrom'];
        $going_to = $_REQUEST['goingto'];
        $category = $_REQUEST['seat_category'];
        $dept_time = $_REQUEST['takeTime'];
        $arri_time = $_REQUEST['landTime'];
        $fare = $_REQUEST['fare'];

        $sql ="INSERT INTO businfo (tourdate, busName, source, destination, busType, dept_time, arri_time, fare_per_seat, avai_seat) VALUES ('$date', '$b_name', '$dept_from', '$going_to', '$category', '$dept_time', '$arri_time', '$fare', 41)";
        if(!mysql_query($sql))
          {
            $err = '<h3 style="color:red">Trip Not Added, Please do it again </h3>';
          }
        else 
          $err = '<h3 style="color:green">Trip Successfully Added</h3>';
      }
?>
<html>
<head>
<title>TourBus | Online Bus reservation</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="febe/style.css" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="../css/responsive.css">
<script src="argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>
<script src="js/application.js" type="text/javascript" charset="utf-8"></script>	
<!--sa poip up-->
<link href="src/facebox.css" media="screen" rel="stylesheet" type="text/css" />
   <script src="lib/jquery.js" type="text/javascript"></script>
  <script src="src/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'src/loading.gif',
        closeImage   : 'src/closelabel.png'
      })
    })
  </script>
</head>
<body>
	<div id="container">
		<div id="adminbar-outer" class="radius-bottom">
			<div id="adminbar" class="radius-bottom">
				<a id="logo" href="index.php"></a>
				<div id="details">
					<a class="avatar" href="javascript: void(0)">
					<img width="36" height="36" alt="avatar" src="img/avatar.jpg">
					</a>
					<div class="tcenter">
					Hi
					<strong>Admin</strong>
					!
					<br>
					<a class="alightred" href="../home.php">Logout</a>
					</div>
				</div>
			</div>
		</div>
		<div id="panel-outer" class="radius" style="opacity: 1;">
			<div id="panel" class="radius">
				<ul class="radius-top clearfix" id="main-menu">
					<li>
						<a class="active" href="index.php">
							<img alt="Dashboard" src="img/m-dashboard.png">
							<span>Dashboard</span>
						</a>
					</li>
					<div class="clearfix"></div>
				</ul>
				<div id="content" class="clearfix">
						<h1 align="center">Available Trips</h1>
					<label for="filter">Filter</label> <input type="text" name="filter" value="" id="filter" />
					<table cellpadding="1" cellspacing="1" id="resultTable">
						<thead>
							<tr>
								<th style="border-left: 1px solid #C1DAD7">Journey Date</th>
								<th>Bus Name </th>
								<th>Routes</th> 
								<th>Category</th>
								<th>Dept.Time</th> 
								<th>Arri.Time</th>
								<th>Avai.Seats</th>
								<th>Trips Delete</th>
							</tr>
						</thead>
						<tbody>
						<?php        
								$sql = "select * from businfo";
								$result = mysql_query($sql);
								$i = 0;        
						
							while($row = mysql_fetch_array($result)) {
									$_SESSION["bus_id"]=$row['busID'];
									echo '<tr class="record">';
									echo '<td style="border-left: 1px solid #C1DAD7;">'.$row['tourdate'].'</td>';
									echo '<td><div align="right">'.$row['busName'].'</div></td>';
									echo '<td><div align="right">'.$row['source']." - ".$row['destination'].'</div></td>';
									echo '<td><div align="right">'.$row['busType'].'</div></td>';
									echo '<td><div align="right">'.$row['dept_time'].'</div></td>';
									echo '<td><div align="right">'.$row['arri_time'].'</div></td>';
									echo '<td><div align="right">'.$row['avai_seat'].'</div></td>';
									echo '<td><div align="center"><a href="busTripDelete.php?id='.$row['busID'].'" class="delbutton" title="Click To Delete" style="cursor:hand;">Delete Trip</a></div></td>';
									echo '</tr>';
								}
							?> 
						</tbody>
					</table>
				</div>
				<div id="content" class="clearfix">
					<h1 align="center">Add Trip</h1>
					<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST">
					<table cellpadding="1" cellspacing="1" id="resultTable">
						<thead>
							<tr>
								<th style="border-left: 1px solid #C1DAD7">Date</th>
								<th>Bus Name </th>
								<th>From</th> 
								<th>To</th>
								<th>Action</th> 
							</tr>
						</thead>
						<tbody>
								<td style="border-left: 1px solid #C1DAD7;"><input type="text" name="datepicker" id="datepicker" required /></td>
								<td><input type="text" name="busName" required /></td>
								<td><input type="text" name="deptfrom" required /></td> 
								<td><input type="text" name="goingto" required /></td>
								<td rowspan="3"><input type="submit" name="submit" value="Add Trips" style="background-color: purple; color: white; font-size: 20px;"/></td>
						</tbody>
						<thead>
							<tr>
								<th style="border-left: 1px solid #C1DAD7">Category</th>
								<th>Dept.Time </th>
								<th>Arri.Time</th> 
								<th>Fare/Seat</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
								<td style="border-left: 1px solid #C1DAD7;">
									<select name="seat_category" required>
										<option disabled="disabled" selected="selected">Select a Category</option>
										<option value="AC Sleeper">AC Sleeper</option>
										<option value="NonAC Sleeper">Non-AC Sleeper</option>
										<option value="AC Semi-Sleeper">AC Semi-Sleeper</option>
										<option value="NonAC Semi-Sleeper">Non-AC Semi-Sleeper</option>
									</select>
								</td>
								<td id="time" class="ui-widget-content"><input type="text" name="takeTime" id="takeTime" required /></td>
								<td id="time" class="ui-widget-content"><input type="text" name="landTime" id="landTime" required /></td> 
								<td><input type="text" name="fare" required /></td>
								<td></td>
							</tbody>
					</table>
					</form>
					<span> <?php echo $err; ?> </span>
				</div>
				<div id="footer" class="radius-bottom">
					2016
					<a class="afooter-link" href="">TourBus</a>
					by
					<a class="afooter-link" href="">Kavitha Megalingam</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<script src="js/jquery.js"></script>
  <script type="text/javascript">
$(function() {


$(".delbutton").click(function(){

//Save the link in a variable called element
var element = $(this);

//Find the id of the link that was clicked
var del_id = element.attr("id");

//Built a url to send
var info = 'id=' + del_id;
 if(confirm("Sure you want to delete this update? There is NO undo!"))
		  {

 $.ajax({
   type: "GET",
   url: "deleteres.php",
   data: info,
   success: function(){
   
   }
 });
         $(this).parents(".record").animate({ backgroundColor: "#fbc7c7" }, "fast")
		.animate({ opacity: "hide" }, "slow");

 }

return false;

});

});
</script>

<script>
  $(document).ready(function() {
    $("#datepicker").datepicker({
          dateFormat: 'dd/mm/yy',
          minDate: 0
        });
    $(document).ready(function(){
            $('#time input').ptTimeSelect();
        });
  });
  </script>
  
  <link href="../date/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="../date/jquery.min.js"></script>
<script src="../date/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../time_JS/jquery.ptTimeSelect.css" />
<script src="../time_JS/jquery.ptTimeSelect.js"></script>
</body>
</html>